<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">CERTIFICATION & AWARDS</p>
    		<p class="breadcrumbs">
    			HOME / ABOUT US / <span class="bread_active">CERTIFICATION & AWARDS</span>
    		</p>
    	</div>
    </div>
    <div class="clearfix"></div>

    <div class="certiOuter">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="certiOuterBox">
                        <div class="col-md-12">
                            <p class="title_1 with_line">CERTIFICATION</p>
                            <div class="_line_"><hr></div>
                        </div>

                        <div class="certiMainBox">
                            <div class="col-md-3 bor_right">
                                <div class="certiLeftBox" id="certiBox1">
                                    <div class="leftBoxTitle">ISO/TS 16949:2009</div>
                                    <div class="leftDesc">
                                        Defines the quality management system requirements for the design and development, production and
                                    </div>
                                </div>
                                <div class="certiLeftBox" id="certiBox2">
                                    <div class="leftBoxTitle">ISO 9001:2008</div>
                                    <div class="leftDesc">
                                        Defines the quality management system requirements for the design and development, production and
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="certiRightBox" id="certiBox11">
                                    <div class="col-md-9">
                                        <div class="rightBoxTitle">
                                            ISO/TS 16949:2009(1)
                                        </div>
                                        <div class="rightBoxDesc">
                                            Needs to demonstrate its ability to consistently provide product that meets customer and 
                                            applicable statutory and regulatory requirements, and aims to enhance customer satisfaction 
                                            through the effective application of the system, including processes for continual improvement 
                                            of the system and the assurance of conformity to customer and applicable statutory and 
                                            regulatory requirements.
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <div class="rightBoxPic"><img src="img/certiPic1.png" width="100%"></div>
                                    </div>
                                </div>

                                <div class="certiRightBox" style="display:none;" id="certiBox21">
                                    <div class="col-md-9">
                                        <div class="rightBoxTitle">
                                            ISO/TS 16949:2009(2)
                                        </div>
                                        <div class="rightBoxDesc">
                                            Needs to demonstrate its ability to consistently provide product that meets customer and 
                                            applicable statutory and regulatory requirements, and aims to enhance customer satisfaction 
                                            through the effective application of the system, including processes for continual improvement 
                                            of the system and the assurance of conformity to customer and applicable statutory and 
                                            regulatory requirements.
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <div class="rightBoxPic"><img src="img/certiPic1.png" width="100%"></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="certiOuterBox">
                        <div class="col-md-12">
                            <p class="title_1 with_line">AWARDS</p>
                            <div class="_line_"><hr></div>
                        </div>

                        <div class="certiMainBox">
                            <div class="col-md-3 bor_right">
                                <div class="certiLeftBox" id="certiBox3">
                                    <div class="leftBoxTitle">ACMA Export Award<br>1999 - 2000</div>
                                </div>
                                <div class="certiLeftBox" id="certiBox4">
                                    <div class="leftBoxTitle">ACMA Export Award<br>2000 - 2001</div>
                                </div>
                                <div class="certiLeftBox" id="certiBox5">
                                    <div class="leftBoxTitle">ACMA Export Award<br>2002 - 2003</div>
                                </div>
                                <div class="certiLeftBox" id="certiBox6">
                                    <div class="leftBoxTitle">ACMA Export Award<br>2003 - 2004</div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="certiRightBox1" id="certiBox31">
                                    <div class="col-md-9">
                                        <div class="rightBoxTitle">
                                            ISO/TS 16949:2009(1)
                                        </div>
                                        <div class="rightBoxDesc">
                                            Needs to demonstrate its ability to consistently provide product that meets customer and 
                                            applicable statutory and regulatory requirements, and aims to enhance customer satisfaction 
                                            through the effective application of the system, including processes for continual improvement 
                                            of the system and the assurance of conformity to customer and applicable statutory and 
                                            regulatory requirements.
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <div class="rightBoxPic"><img src="img/certiPic2.png" width="100%"></div>
                                    </div>
                                </div>

                                <div class="certiRightBox1" style="display:none;" id="certiBox41">
                                    <div class="col-md-9">
                                        <div class="rightBoxTitle">
                                            ISO/TS 16949:2009(2)
                                        </div>
                                        <div class="rightBoxDesc">
                                            Needs to demonstrate its ability to consistently provide product that meets customer and 
                                            applicable statutory and regulatory requirements, and aims to enhance customer satisfaction 
                                            through the effective application of the system, including processes for continual improvement 
                                            of the system and the assurance of conformity to customer and applicable statutory and 
                                            regulatory requirements.
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <div class="rightBoxPic"><img src="img/certiPic2.png" width="100%"></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="certiOuterBox">
                        <div class="col-md-12">
                            <p class="title_1 with_line">GUARANTEE POLICY</p>
                            <div class="_line_"><hr></div>
                        </div>

                        <div class="certiMainBox">
                            <div class="col-md-9">
                                <div class="rightBoxDesc">
                                    We guarantee that all the goods that have been manufactured by us are free from defects in material 
                                    and workmanship. We further guarantee that all the parts that are defective in material and workmanship 
                                    are repairable and/or interchangeable. Any replaced parts will be our property. The obligation under 
                                    this warranty does not include the refunding of these losses - Loss of profit, assembly expenses, 
                                    customs charges, inland freight, all local charges etc.. We shall not be responsible for the wear 
                                    out and / or the defects that arise in the transit and/or in the case that any alteration is carried 
                                    out on the goods shipped by us, we will not be responsible for the defects weather it is due to the 
                                    alteration or not. We shall also not be liable for the treatment of the shipped goods that is not 
                                    in accordance with our regulations regarding its use. We further warranty that all our goods are 
                                    interchangeable with the corresponding parts offered by the Original Equipment Manufacturer. 
                                    All claims have to be submitted to us in writing within 6 months from the shipment of the goods 
                                    and material may not be returned to us without our prior consent. In any case, they have to be 
                                    returned to us free of charge and freight charges will only be returned if the claim is found justified after inspection of the goods by our quality control department.
                                </div>
                            </div>
                            <div class="col-md-3 text-center">
                              <div class="languageCerti">
                                <img src="img/certiPic3.png" width="100%">
                              </div>  
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
  </body>
</html>    