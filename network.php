<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">NETWORK</p>
    		<p class="breadcrumbs">
    			HOME / <span class="bread_active">NETWORK</span>
    		</p>
    	</div>
    </div>

    <div class="networkSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title_1 with_line">OUR DISTRIBUTION NETWORK</p>
                        <div class="_line_"><hr></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="abTopPicDesc">
                            We have been supplying crankshafts to leading Original Equipment Manufacturers within India and the SAARC 
                            region as well as to the global aftermarket to over 80 countries around the world. We look forward to 
                            further expand our horizons to more markets worldwide & develop a brand with global dominance & a leader 
                            in the industry.<br><br>
                            To get in touch us, please use the Distributor Enquiry form below. We will respond promptly.
                        </div>
                        <div class="readBtn" data-toggle="modal" data-target="#donarModal">BECOME A DISTRIBUTOR</div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="networkImg">
                            <img src="img/networkMap.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
  </body>
</html>  