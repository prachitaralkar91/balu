 <?php include "head.php"; ?>
<div class="container-fluid padding-zero">
    <div id="carousel-example-generic" class="carousel slide">
      <!-- Indicators -->
      <!-- <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol> -->

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">

        <!-- First slide -->
        <div class="item active deepskyblue">

          <div class="carousel-caption">
            <h3 data-animation="animated fadeInLeft">
             <img src="img/slider_effect.png">
            </h3>
            <h3 data-animation="animated fadeInLeft" class="slider_cap">
              <span class="title_1_slider">You demand perfection.<span> <br>
              <span class="title_2_slider">BALU DELIVERS.</span>  <br>
              <span class="title_3_slider">In today’s market, there’s no room for error. That’s why, at Balu India, we’re<br>continuously working to improve quality. It starts with having the latest<br>technology and the most talented employees. But, of course, it’s more than <br>that. It’s a deep-seated commitment to build your parts right the first time,<br> every time, on time. It’s making sure everything we do is built with perfection.</span>
            </h3>
          </div>
        </div> <!-- /.item -->
        <!-- First slide -->
        <div class="item deepskyblue">

          <div class="carousel-caption">
            <h3 data-animation="animated fadeInLeft">
             <img src="img/slider_effect.png">
            </h3>
            <h3 data-animation="animated fadeInLeft" class="slider_cap">
              <span class="title_1_slider">You demand perfection.<span> <br>
              <span class="title_2_slider">BALU DELIVERS.</span>  <br>
              <span class="title_3_slider">In today’s market, there’s no room for error. That’s why, at Balu India, we’re<br>continuously working to improve quality. It starts with having the latest<br>technology and the most talented employees. But, of course, it’s more than <br>that. It’s a deep-seated commitment to build your parts right the first time,<br> every time, on time. It’s making sure everything we do is built with perfection.</span>
            </h3>
          </div>
        </div> <!-- /.item -->
        <!-- First slide -->
        <div class="item deepskyblue">

          <div class="carousel-caption">
            <h3 data-animation="animated fadeInLeft">
             <img src="img/slider_effect.png">
            </h3>
            <h3 data-animation="animated fadeInLeft" class="slider_cap">
              <span class="title_1_slider">You demand perfection.<span> <br>
              <span class="title_2_slider">BALU DELIVERS.</span>  <br>
              <span class="title_3_slider">In today’s market, there’s no room for error. That’s why, at Balu India, we’re<br>continuously working to improve quality. It starts with having the latest<br>technology and the most talented employees. But, of course, it’s more than <br>that. It’s a deep-seated commitment to build your parts right the first time,<br> every time, on time. It’s making sure everything we do is built with perfection.</span>
            </h3>
          </div>
        </div> <!-- /.item -->
      </div><!-- /.carousel-inner -->

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->

</div><!-- /.container -->



   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
  </body>
</html>