/* Demo Scripts for Bootstrap Carousel and Animate.css article
* on SitePoint by Maria Antonietta Perna
*/
var wW = $(window).width();

$(document).ready(function () {
  if(wW >= 768){
    if (window.location.href.indexOf("index") > -1)
    {
        $("#home").css("color","#df0024");
    }
    else if(window.location.href.indexOf("aboutus") > -1){

      $("#aboutus").css("color","#df0024");
    }
    else if(window.location.href.indexOf("certifications") > -1){

      $("#aboutus").css("color","#df0024");
    }
    else if(window.location.href.indexOf("manufacturing") > -1){
      $("#manufacture").css("color","#df0024");
    }
    else if(window.location.href.indexOf("approach") > -1){
      $("#ourapproach").css("color","#df0024");
    }
    else if(window.location.href.indexOf("network") > -1){
      $("#network").css("color","#df0024");
    }
    else if(window.location.href.indexOf("sales_service") > -1){
      $("#service").css("color","#df0024");
    }
    else if(window.location.href.indexOf("contactus") > -1){
      $("#contactus").css("color","#df0024");
    }
  }
}); 


(function( $ ) {

	//Function to animate slider captions 
	function doAnimations( elems ) {
		//Cache the animationend event in a variable
		var animEndEv = 'webkitAnimationEnd animationend';
		
		elems.each(function () {
			var $this = $(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = $('#carousel-example-generic'),
		$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
		doAnimations($animatingElems);
	});  

	 // $("#carousel_approach").owlCarousel({
 		
  //     	loop:true,
	 //    margin:10,
	 //    nav:true,
	 //    responsive:{
	 //        0:{
	 //            items:1
	 //        },
	 //        600:{
	 //            items:2
	 //        },
	 //        1000:{
	 //            items:3
	 //        }
	 //    }
	 
  // });
	
})(jQuery);


// skill hover effect
if(wW >= 768){
$('.skil').hover(
	function() {
   		$('.skil').css('opacity','0.4');
   		$(this).css('opacity','1');
   		var a = $(this).find('img:first').attr('src');
   		$(this).find('img:first').stop().animate({height:"250px",marginTop:"-26px"},200);
   		$(this).find('.homeSect3TxtBox').animate({paddingTop:"20px"},200);
   		$(this).find('button').show();
   		if(a ==  "img/firstHover.png"){
   			$(this).find('img:first').attr('src','img/first.png');
   		}else if(a == "img/second.png"){
   			$(this).find('img:first').attr('src','img/sec_hover.png');
   		}else if(a == "img/third.png"){
   			$(this).find('img:first').attr('src','img/third_hover.png');
   		}
 	}, function() {
    		$('.skil').css('opacity','0.4');
    		var a = $(this).find('img:first').attr('src');
    		$(this).find('img:first').animate({height:"224px",marginTop:"0px"},200);
    		$(this).find('.homeSect3TxtBox').animate({paddingTop:"50px"},200);
    		$(this).find('button').hide();
    		if(a ==  "img/first.png"){
   			$(this).find('img:first').attr('src','img/firstHover.png');
   		}else if(a == "img/sec_hover.png"){
   			$(this).find('img:first').attr('src','img/second.png');
   		}else if(a == "img/third_hover.png"){
   			$(this).find('img:first').attr('src','img/third.png');
   		}
  	}
);


$('.skil1').hover(
	function() {
   		$('.skil1').css('opacity','0.4');
   		$(this).css('opacity','1');
   		var a = $(this).find('img').attr('src');
   		$(this).find('img').stop().animate({height:"250px",marginTop:"-26px"},200);
   		$(this).find('.homeSect3TxtBox').animate({paddingTop:"20px"},200);
   		$(this).find('button').show();
   		if(a ==  "img/abtLogo1.png"){
   			$(this).find('img').attr('src','img/abtLogo1Hover.png');
   		}else if(a == "img/abtLogo2.png"){
   			$(this).find('img').attr('src','img/abtLogo2Hover.png');
   		}else if(a == "img/abtLogo3.png"){
   			$(this).find('img').attr('src','img/abtLogo3Hover.png');
   		}
 	}, function() {
    		$('.skil1').css('opacity','0.4');
    		var a = $(this).find('img').attr('src');
    		$(this).find('img').animate({height:"224px",marginTop:"0px"},200);
    		$(this).find('.homeSect3TxtBox').animate({paddingTop:"50px"},200);
    		$(this).find('button').hide();
    		if(a ==  "img/abtLogo1Hover.png"){
   			$(this).find('img').attr('src','img/abtLogo1.png');
   		}else if(a == "img/abtLogo2Hover.png"){
   			$(this).find('img').attr('src','img/abtLogo2.png');
   		}else if(a == "img/abtLogo3Hover.png"){
   			$(this).find('img').attr('src','img/abtLogo3.png');
   		}
  	}
);

$('.sale_service').hover(
  function() {
      $('.sale_service').css('opacity','0.4');
      $(this).css('opacity','1');
      var a = $(this).find('img').attr('src');
      $(this).find('img').stop().animate({height:"250px",marginTop:"-26px"},200);
      $(this).find('.homeSect3TxtBox').animate({paddingTop:"20px"},200);
      $(this).find('button').show();
      if(a ==  "img/serviceLogo1.png"){
        $(this).find('img').attr('src','img/serviceLogo1Hover.png');
      }else if(a == "img/serviceLogo2.png"){
        $(this).find('img').attr('src','img/serviceLogo2Hover.png');
      }else if(a == "img/serviceLogo3.png"){
        $(this).find('img').attr('src','img/serviceLogo3Hover.png');
      }
  }, function() {
        $('.sale_service').css('opacity','0.4');
        var a = $(this).find('img').attr('src');
        $(this).find('img').animate({height:"224px",marginTop:"0px"},200);
        $(this).find('.homeSect3TxtBox').animate({paddingTop:"50px"},200);
        $(this).find('button').hide();
        if(a ==  "img/serviceLogo1Hover.png"){
        $(this).find('img').attr('src','img/serviceLogo1.png');
      }else if(a == "img/serviceLogo2Hover.png"){
        $(this).find('img').attr('src','img/serviceLogo2.png');
      }else if(a == "img/serviceLogo3Hover.png"){
        $(this).find('img').attr('src','img/serviceLogo3.png');
      }
    }
);
}

$(".certiLeftBox").click(function(){
  var getId = $(this).attr('id');
  $(".leftBoxTitle").removeClass("activeMenu");
  $(this).find(".leftBoxTitle").addClass("activeMenu");
  if ($("#"+getId+1).hasClass('certiRightBox')){
    $(".certiRightBox").css("display","none");
    $("#"+getId+1).fadeIn( "slow", function() {
    
  });
  }
  else{
    $(".certiRightBox1").css("display","none");
    $("#"+getId+1).fadeIn( "slow", function() {
    
  });
  }
  //$(".certiRightBox").css("display","none");
  
});

$(".contactTitleGrid").click(function(){
    var offcId = $(this).attr('id');
    $(".contactTitleGrid").removeClass("officeActive");
    $("#"+offcId).addClass("officeActive");
    $(".contactOfficeGrid").css("display","none");
    $("#"+offcId+1).fadeIn( "slow", function() {
    
  });
  
  //$(".certiRightBox").css("display","none");
  
});

function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(19.1839458,72.8515168),
    zoom:14,
    mapTypeControl: false,
    scrollwheel:false,

    mapTypeId:google.maps.MapTypeId.ROADMAP
  };

  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);

  var marker = new google.maps.Marker({
    position: {lat: 19.1839458 , lng: 72.8515168},
    map: map,
    title: 'HSE INTEGRO!'
  });
}
google.maps.event.addDomListener(window, 'load', initialize);


function openRMmodal(id){
 // alert(id);
 $("#rm_Modal h3").html("");
 $("#rm_Modal .sub_txt").html("");
 $("#rm_Modal").find('img').attr('src', "");
 $("#rm_Modal").find('img').css("display", "none");
  var pic = "";
    var title = $("#"+id+1).find("h3").html();
    var desc = $("#"+id+1).find(".sub_txt").html();
    //alert(desc);

    pic = $("#"+id+1).find('.rdImg').find("img").attr('src');
    //alert(pic);
     $("#rm_Modal").modal("show");
     $("#rm_Modal").on('shown.bs.modal', function () {
      //alert("in");
      try{
        if(pic != undefined){
        //alert(pic);
        $("#rm_Modal").find('img').css("display", "block");
       // $("#rm_Modal").find('img').css("height", "150px");
        $("#rm_Modal").find('img').attr('src', pic);
        $("#rm_Modal").find("h3").html(title);
        $("#rm_Modal").find(".sub_txt").html(desc);
        
      }
      else{
         $("#rm_Modal").find('img').css("display", "none");
        //$("#rm_Modal").find('img').css("height", "0px");
        $("#rm_Modal").find("h3").html(title);
        $("#rm_Modal").find(".sub_txt").html(desc);
       }
      }
      catch(Exception ){
        alert("Error "+ e);
      }
      
       // alert(prodName);
    });
}

$('#modal_body').slimScroll({
      allowPageScroll: true,
      height:"420px"

  });
