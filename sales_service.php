<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">AFTER SALES SERVICE</p>
    		<p class="breadcrumbs">
    			HOME / <span class="bread_active">AFTER SALES SERVICE</span>
    		</p>
    	</div>
    </div>

    <div class="clearfix"></div>
    <div class="saleSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title_1 with_line">OUR SERVICE</p>
                        <div class="_line_"><hr></div>
                    </div>
                    <div class="col-md-12">
                        <div class="abTopPicDesc text-center">
                            An excellent & efficient after Sales Service to the customer 
                            ensures customer satisfaction & very high customer retention. 
                            Our After sales service executive can be reached on 
                            <a href="mailto:support@baluindustries.com" style="color:#e11132; text-decoration:none;">support@baluindustries.com.</a>
                             
                            Our offering include
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <div class="homeSect3">
      <div class="container">
        <div class="homeSect3Inner">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4 sale_service" style="opacity: 0.4;">
                <div class="col-md-4">
                  <img id="awardimg" src="img/serviceLogo1.png" style="text-align:center;">
                </div>
                <div class="col-md-8 homeSect3TxtBox" style="padding-top: 50px; margin-left:-30px;">
                  <p class="welcome forfont1" style="width:285px;">Warranty Management</p>
                  <p style="width:285px;">
                    We ensure there is a warranty management system in place to ensure 
                    any customer’s request in reference to a warranty claim or query is 
                    attended to & a decision on the same 
                  </p>
                  <button class="btn btn-danger" id="rm19" onclick="openRMmodal(this.id);" style="color: rgb(204, 0, 0); padding-top: 3px; padding-bottom: 3px; border-radius:0px; display: none; background-color: rgb(255, 255, 255);">
                    Read More</button>
                  </a>
                </div>

                <div class="readMoreInfo" id="rm191" style="display:none;">
                  <h3>Warranty Management</h3>
                  <div class="clearfix"></div>
                  
                  <div class="sub_txt">
                    We ensure there is a warranty management system in place to ensure 
                    any customer’s request in reference to a warranty claim or query is 
                    attended to & a decision on the same is taken within 24 hours from 
                    time of raising of the claim.                  
                  </div>
                </div>
              </div>

              <div class="col-md-4 sale_service" style="opacity: 0.4;">
                <div class="col-md-4" style="text-align:center">
                  <img id="crsimg" src="img/serviceLogo2.png">
                </div>
                <div class="col-md-8 homeSect3TxtBox" style="padding-top: 50px; margin-left:-30px;">
                  <p class="welcome forfont1" style="width:285px;">Swift Response Time</p>
                  <p style="width:285px;">
                    We believe that a Swift Response time is key to After Sales service 
                    initiative to ensure the customer receives prompt feedback to any 
                    query or question raised. 
                  </p>
                  <button class="btn btn-danger" id="rm20" onclick="openRMmodal(this.id);" style="color: rgb(204, 0, 0); padding-top: 3px; border-radius:0px; padding-bottom: 3px; display: none; background-color: rgb(255, 255, 255);">
                    Read More</button>
                </div>
                <div class="readMoreInfo" id="rm201" style="display:none;">
                  <h3>Swift Response Time</h3>
                  <div class="clearfix"></div>
                  
                  <div class="sub_txt">
                    We believe that a Swift Response time is key to After Sales service 
                    initiative to ensure the customer receives prompt feedback to any 
                    query or question raised. We have a committed response time of 2 
                    hours from time of raising the same.
                  </div>
                </div>
              </div>

              <div class="col-md-4 sale_service" style="opacity: 0.4;" >
                <div class="col-md-4" style="text-align:center">
                  <img id="newsimg" src="img/serviceLogo3.png">
                </div>
                <div class="col-md-8 homeSect3TxtBox" style="padding-top: 50px; margin-left:-30px;">
                  <p class="welcome forfont1" style="width:285px;">Customer Relationship Management</p>
                  <p style="width:285px;">
                    We target & commit to meet customer expectations with uncompromising 
                    quality standards, flexibility, reliability and willingness to 
                    innovate. 
                  </p>
                  <button class="btn btn-danger" id="rm21" onclick="openRMmodal(this.id);" style="color: rgb(204, 0, 0); padding-top: 3px; border-radius:0px; padding-bottom: 3px; display: none; background-color: rgb(255, 255, 255);">
                    Read More</button>
                </div>

                <div class="readMoreInfo" id="rm211" style="display:none;">
                  <h3>Customer Relationship Management</h3>
                  <div class="clearfix"></div>
                  
                  <div class="sub_txt">
                    We target & commit to meet customer expectations with uncompromising 
                    quality standards, flexibility, reliability and willingness to 
                    innovate. We believe in setting the highest standard for every 
                    customer’s journey resulting in a high customer lifecycle value 
                    & 100% Customer retention.
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include "footer.php"; ?>
  </body>
</html>  