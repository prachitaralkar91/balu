<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">OUR APPROACH</p>
    		<p class="breadcrumbs">
    			HOME / ABOUT US / <span class="bread_active">APPROACH</span>
    		</p>
    	</div>
    </div>

    <div class="clearfix"></div>

    <div class="approachSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title_1 with_line">BALU APPROACH</p>
                        <div class="_line_"><hr></div>
                    </div>
                    <div class="col-md-12">
                        <div class="abTopPicDesc text-center">
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                            a galley of type and <br>scrambled it to make a type specimen book.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="processSectOuter">
                        <div class="col-md-3">
                            <img src="img/approachPic1.png" width="100%" height="150">
                        </div>
                        <div class="col-md-9">
                            <div class="processTitle">
                                Technology
                            </div>
                            <div class="abTopPicDesc">
                                ‘BALU’, is now an avant garde manufacturer of fully finished and semi-finished forged crankshafts. 
                                The machining of the crankshafts is done with the latest equipments, instruments, technologies and 
                                highly skilled workforce which provides exceptional control over the entire process of manufacturing 
                                the crankshafts and has made ‘Balu’ the supplier of choice of major OEM's not only in India but around 
                                the world due to our technological advantage and the highest standards of quality in the industry. 

                            </div>
                            <div class="readBtn" id="rm7" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>
                        <div class="readMoreInfo" id="rm71" style="display:none;">
                          <h3>TECHNOLOGY</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                            <img src="img/approachPic1.png" width="100%" height="150">
                        </div>
                          <div class="sub_txt">
                            ‘BALU’, is now an avant garde manufacturer of fully finished 
                            and semi-finished forged crankshafts. The machining of the 
                            crankshafts is done with the latest equipments, instruments, 
                            technologies and highly skilled workforce which provides 
                            exceptional control over the entire process of manufacturing 
                            the crankshafts and has made ‘Balu’ the supplier of choice 
                            of major OEM's not only in India but around the world due to 
                            our technological advantage and the highest standards of 
                            quality in the industry. All the crankshafts are manufactured 
                            to exact O.E. specifications and on CNC Auto lines, to 
                            ensure precision at every stage. ‘Balu’ is committed to the 
                            goal of being recognized globally as a leader through 
                            engineering excellence. The group has always been at the 
                            fore-front of technology and is known for its investments 
                            in technology ahead of time. Over the years, it has been 
                            investing in creating state-of-the-art facilities, 
                            world-class capacities and capabilities. It has adopted 
                            the latest technologies in all its processes, which has 
                            helped garner significant market share globally. Today, 
                            our facilities include fully automated lines, the largest 
                            of its kind and among the best in the industry. <br><br>
                            A brief of our technological advantage can be seen below:<br>
                            <ul>
                                <li>Infrastructural Capacity to Manufacture 1000 Crankshafts every day</li>
                                <li>Fully Engineered and Equipped to OEM requirements</li>
                                <li>In House Standard Room with CMM Capability</li>
                                <li>Facing & Geometrically Centering on Endomatic</li>
                                <li>Mass Centring on Schenck</li>
                                <li>PIN Milling on Heller & Journal on CNC Turning Centers</li>
                                <li>Oil holes on Thomas Ryder & SPM for universal oil hole</li>
                                <li>Induction Hardening on solid state AEG Elotherm</li>
                                <li>In-house Metalogical Lab to check hardness, Case depth and Material composition</li>
                                <li>Grinding on CNC Journal, Pin & Angular Newall / Landis Grinders</li>
                                <li>Post Grinding operations on Makino Horizontal Machining Centers</li>
                                <li>Dynamic Balancing on Schenk/Abro</li>
                                <li>Super Finishing on Nagel Auto lapping</li>
                                <li>Pressure Cleaning on Otto Durr</li>
                            </ul>
                          </div>
                        </div>
                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-9">
                            <div class="processTitle">
                                Innovation
                            </div>
                            <div class="abTopPicDesc">
                                ‘Balu’ has incremental innovation as a fore-front of the R&D with constant commitment and interest in 
                                the application of the new technologies. The company strives to be a leader in every aspect of its 
                                business. The Spirit of Innovation fuels us to aggressively grow our businesses by accessing global 
                                markets, to deliver products and services of uncompromising quality, consistent with the ‘Balu’ brand 
                                and image.Innovation being at our heart, we continue our endeavour of rising through diversification 
                                & manufacturing high quality crankshafts that drive change and sustainability. We are committed to our 
                                promise of pushing forward and striving towards greater heights with each passing year & achieving global 
                                dominance in the field of
                            </div>
                            <div class="readBtn" id="rm8" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>
                        <div class="col-md-3">
                            <img src="img/approachPic2.png" width="100%" height="150">
                        </div>

                        <div class="readMoreInfo" id="rm81" style="display:none;">
                          <h3>Innovation</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                                <img src="img/approachPic2.png" width="100%" height="150">
                            </div>
                          <div class="sub_txt">
                            ‘Balu’ has incremental innovation as a fore-front of the R&D 
                            with constant commitment and interest in the application of 
                            the new technologies. The company strives to be a leader in 
                            every aspect of its business. The Spirit of Innovation fuels 
                            us to aggressively grow our businesses by accessing global 
                            markets, to deliver products and services of uncompromising 
                            quality, consistent with the ‘Balu’ brand and image. Innovation 
                            being at our heart, we continue our endeavour of rising 
                            through diversification & manufacturing high quality 
                            crankshafts that drive change and sustainability. 
                            We are committed to our promise of pushing forward and 
                            striving towards greater heights with each passing year 
                            & achieving global dominance in the field of crankshaft 
                            manufacturing.
                          </div>
                        </div>
                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-3">
                            <img src="img/approachPic3.png" width="100%" height="150">
                        </div>
                        <div class="col-md-9">
                            <div class="processTitle">
                                New Product Development
                            </div>
                            <div class="abTopPicDesc">
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer 
                                took a galley of type and scrambled it to make a type specimen book. It has survived not only five 
                                centuries, but also the leap into electronic typesetting, remaining essentially unchanged Lorem Ipsum 
                                has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley 
                                of type and scrambled it to make a type specimen book. It has survived not only five centuries, but 
                                also the leap into electronic typesetting, remaining essentially unchanged
                            </div>
                            <div class="readBtn" id="rm9" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>

                        <div class="readMoreInfo" id="rm91" style="display:none;">
                          <h3>New Product Development</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                          <img src="img/approachPic3.png" width="100%" height="150">
                            </div>
                          <div class="sub_txt">
                            <b>1) Concurrent Engineering</b><br><br>

                            <b>Customer Drawing </b><br>
                            Crankshafts of any applications can be made according to 
                            drawing or sample. Through these customer specifications 
                            we receive the relevant data required for development.<br><br>

                            <b>CAD/3D Model</b><br>
                            The crankshaft drawings & specifications are developed 
                            in our CAD or 3D software.<br><br>

                            <b>Development Of Processes</b><br>
                            The company has gained expertise in manufacturing of 
                            crankshafts for more than 25 years in respect to process 
                            of material selection, heat treatment, surface treatment etc.<br><br>

                            <b>Testing of Material</b> <br>

                            Material testing is done in our R&D & Metallurgical laboratory. 
                            The Use of materials is decided depending upon the chemical 
                            composition as per engine running type or if specified in 
                            Customer drawing.<br><br>

                            <b>Development of Prototype</b><br>
                            We have a prototype division where entire process of 
                            manufacturing is done on a parallel & flexible line without 
                            affecting the series of production.<br>

                            a) Forging: The crankshafts are forged in our 16000 T 
                            hammers & up to a length of 2.5 metres. The crankshafts 
                            undergo a heat treatment process (H & T) & 100% Crack 
                            testing (Magnaflux) before we initiate machining of 
                            the same.<br><br>

                            b) Machining: All machining such as Turning, drilling, milling, 
                            threading, etc are done on our    automated CNC machines.<br><br>

                            c) Dynamic Balancing & Final Assembly: Finally counterweights
                             & gears are assembled and dynamically balanced. Then the 
                             crankshafts are ready to install on on the engine.<br><br>

                            <b>Crankshaft Testing</b><br>
                            The company has an in-house testing centre & R&D laboratory 
                            where crankshafts are subject to various tests & the rests 
                            are then collected to under understand the future performance 
                            of the crankshafts<br><br>

                            <b>Preparation of Drawings/Layouts</b><br>
                            As the conclusion of our process of Concurrent Engineering, 
                            there is a complete documented solution in terms of 
                            drawings/layouts for the production process<br><br>

                            <b>2) Production Process:</b><br><br>

                            <b>Die Design & Fabrication:</b><br>
                            The Design & Fabrication of all type of dies are done through 
                            exporting the 3D Model into our Die machining centre.<br><br>

                            <b>Forging:</b><br>
                            The crankshafts are forged in our 16000 T hammers & up to a 
                            length of 2.5 metres. The crankshafts undergo a heat treatment 
                            process (H & T) & 100% Crack testing (Magnaflux) before we 
                            initiate machining of the same.<br><br>

                            <b>Heat Treatment:</b><br>
                            The crankshafts undergo a heat treatment process (H & T) & 
                            100% Crack testing (Magnaflux) before we initiate machining 
                            of the same.<br><br>

                            <b>Material composition:</b><br>
                            The material composition & properties are perfectly checked 
                            prior to machining in our R&D Laboratory & testing centre.<br><br>

                            <b>Rough machining:</b><br>
                            Initially Facing & Centering, Turning & Milling & Oil Hole 
                            Drilling is carried out on the CNC Machines in the Rough 
                            Machining Unit.<br><br>

                            <b>Finish Machining:</b><br>
                            The Finish Machining Unit consists of CNC grinding, dynamic 
                            balancing, post grinding operations, superfinishing & 
                            accessory Product and Assembly.<br><br>

                            <b>Quality Control</b><br>
                            The R&D Laboratory & Testing Centre consisting of metallurgical 
                            testing, Crack Testing, Fatigue/durability testing, Dimensional
                             testing, etc ensures that every crankshaft leaving our shop 
                             floor is 100% quality checked.<br><br>



                          </div>
                        </div>
                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-9">
                            <div class="processTitle">
                                Future Developments
                            </div>
                            <div class="abTopPicDesc">
                                Our 25 years of experience in the manufacturing of engine 
                                components, ‘Balu’ has strived to offer end to end solutions
                                 to its customers and with the present infrastructure 
                                 capability & strong financial support, the company has 
                                 ventured into manufacturing other engine components for 
                                 specific customer on ‘Made to Order’ (MTO) basis.                            </div>
                            <div class="readBtn" id="rm10" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>
                        <div class="col-md-3">
                            <img src="img/approachPic4.png" width="100%" height="150">
                        </div>

                        <div class="readMoreInfo" id="rm101" style="display:none;">
                          <h3>Future Developments</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                          <img src="img/approachPic4.png" width="100%" height="150">
                            </div>
                          <div class="sub_txt">
                            Our 25 years of experience in the manufacturing of engine 
                            components, ‘Balu’ has strived to offer end to end solutions 
                            to its customers and with the present infrastructure 
                            capability & strong financial support, the company has 
                            ventured into manufacturing other engine components for 
                            specific customer on ‘Made to Order’ (MTO) basis.<br><br>

                            The company has also assisted companies with Assembling of 
                            certain engine parts to offer a more convenient solution to 
                            some of our customers<br><br>

                            We are also looking forward to add Cylinder Heads, Cylinder 
                            Blocks and Camshaft amongst a few to our manufacturing range.            

                          </div>
                        </div>

                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-3">
                            <img src="img/approachPic1.png" width="100%" height="150">
                        </div>
                        <div class="col-md-9">
                            <div class="processTitle">
                                Environment
                            </div>
                            <div class="abTopPicDesc">
                                ‘Balu’ is committed to preserving & conservation of the 
                            environment as a step towards creation of a ‘green factory’ 
                            in harmony with the local area an important part of our 
                            environmental policy. The environmental policy revolves 
                            around the implementation and maintenance of an Environmental 
                            Management System & we are in the process of receiving the 
                            ISO/TS 14001 accreditation for the same.
                            </div>
                            <div class="readBtn" id="rm11" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>
                            
                        <div class="readMoreInfo" id="rm111" style="display:none;">
                          <h3>Environment</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                          <img src="img/approachPic1.png" width="100%" height="150">
                      </div>
                          <div class="sub_txt">
                            ‘Balu’ is committed to preserving & conservation of the 
                            environment as a step towards creation of a ‘green factory’ 
                            in harmony with the local area an important part of our 
                            environmental policy. The environmental policy revolves 
                            around the implementation and maintenance of an Environmental 
                            Management System & we are in the process of receiving the 
                            ISO/TS 14001 accreditation for the same. As part of our 
                            environmental management system, we are carrying out an 
                            internal audit as well as regular external audits to strive 
                            to increase the environmental awareness of our staff & speed 
                            up our goal of being a ‘green factory’.<br><br>
                            We are engaging in implementing a strong & sustainable 
                            waste management system as part of our environmental policy
                             to minimize our environmental footprint and to optimize 
                             resources through energy-saving and efficiency measures. 
                             This is borne out by the actions planned in our energy 
                             management system, recycling and reuse of resources. By 
                             constantly applying improvements and new procedures we 
                             are able to progress towards an ‘innovative’ & ‘go green’ 
                             industrial model committed to the conservation of the 
                             environment.
                          </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
  </body>
</html>    