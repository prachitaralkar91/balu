<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">CONTACT US</p>
    		<p class="breadcrumbs">
    			HOME / <span class="bread_active">CONTACT US</span>
    		</p>
    	</div>
    </div>

    <div class="contactGridSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="contactOfficeOuter">
                            <div class="contactTitleGrid officeActive" id="office1">
                                corporate office
                            </div>
                            <div class="contactTitleGrid" id="office2">
                                Plant Address (Unit 1)
                            </div>
                            <div class="contactTitleGrid" id="office3">
                                Plant Address (Unit 2)
                            </div>
                            <div class="contactTitleGrid" id="office4">
                                Plant Address (Unit 3)
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="contactOfficeGrid" id="office11">
                        <div class="col-md-6">
                            <div class="officePic"><img src="img/contactPic1.png" width="100%"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="contactoffInfoBox">
                                <div class="officeInfoTitle">
                                    Corporate Office
                                </div>
                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactHomeIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        Suite No.506 / 507, Hotel Imperial Palace, 45,
                                        Telly Park Road, Andheri (E), Mumbai-400069 (India).
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        +91-22-26839916, 66701608
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon2.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        +91-22-26842860
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactMailIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        balucrank@gmail.com / sales@baluindustries.com
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contactOfficeGrid" id="office21" style="display:none;">
                        <div class="col-md-6">
                            <div class="officePic"><img src="img/contactPic1.png" width="100%"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="contactoffInfoBox">
                                <div class="officeInfoTitle">
                                    Unit No. 1
                                </div>
                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactHomeIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        43-B Kakti Industrial Area, Kakti, Belgaum-591113, Karnataka, (India)
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        + 91-831-2470705, 2476186
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon2.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        + 91-831-2470013 
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactMailIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        <!-- balucrank@gmail.com / sales@baluindustries.com -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contactOfficeGrid" id="office31" style="display:none;">
                        <div class="col-md-6">
                            <div class="officePic"><img src="img/unit2.jpg" width="100%"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="contactoffInfoBox">
                                <div class="officeInfoTitle">
                                    Unit No. 2
                                </div>
                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactHomeIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        44-A Kakti Industrial Area, Kakti, Belgaum-591113, Karnataka, (India)
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        + 91-831-2470705, 2476186
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon2.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        + 91-831-2470013 
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactMailIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        <!-- balucrank@gmail.com / sales@baluindustries.com -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contactOfficeGrid" id="office41" style="display:none;">
                        <div class="col-md-6">
                            <div class="officePic"><img src="img/unit3_img.jpg" width="100%"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="contactoffInfoBox">
                                <div class="officeInfoTitle">
                                    Unit No. 3
                                </div>
                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactHomeIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        R S No. 690 – Udyambag, Belgaum – 590008. Karnataka, (India)
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        <!-- +91-22-26839916, 66701608 -->
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactFaxIcon2.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        <!-- +91-22-26842860 -->
                                    </div>
                                </div>

                                <div class="conInfoInnerBox">
                                    <div class="conInnerBoxLogo">
                                        <img src="img/contactMailIcon.png">
                                    </div>
                                    <div class="conInfoDesc">
                                        <!-- balucrank@gmail.com / sales@baluindustries.com -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contactFormSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="form_section">
                    <form action="contact_post.php" name="contact_form" id="contact_form" method="POST">
                    <h3 text-center="">Write To Us</h3>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            
                            <div class="form-group">
                                <input type="Name" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="MobileNo" class="form-control" name="phone" id="phone" placeholder="Mobile No.">
                            </div>
                            <div class="form-group">
                                <input type="OrganizationName" class="form-control" name="organization" id="organization" placeholder="Organization Name">
                            </div>
                            <div class="form-group">
                                <input type="EmailId" class="form-control" name="email" id="email" placeholder="Email Id.">
                            </div>
                            
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            
                            <div class="form-group">
                                <textarea class="form-control" rows="6" name="message" id="message" placeholder="Query"></textarea>
                            </div>
                        </div>  

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <button type="button" class="btn btn-default custom_submit_btn" name="send_form" id="send_form">SUBMIT</button>
                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="googleMapSect">
        <div id="googleMap" ></div>
    </div>
    <?php include "footer.php"; ?>
  </body>
</html>  