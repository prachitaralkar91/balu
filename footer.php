<footer>
	<div class="footer_wrap">
		<div class="container-fluid">
			<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="col-md-6 col-sm-6 col-xs-12 footer_contents">
				<div class="footer_contents">
					<p class="footer_title">GET IN TOUCH
					</p>
					<p class="sub_content_foot">
						For general enquiries,<br>
						please contact us using the below
					</p>
				</div>
				<div class="footer_contents">
					<ol>
						<li><i class="fa fa-phone fa-2x footer_icons" aria-hidden="true"></i>+91-22-6785 2000</li>
						<li><i class="fa fa-envelope-o fa-2x footer_icons" aria-hidden="true"></i>Send us a message</li>
						<li><i class="fa fa-video-camera fa-2x footer_icons" aria-hidden="true"></i>Video Conference</li>
					</ol>
				</div>
			</div>
			<div class="col-md-6 footer_contents">
					<p class="footer_title">FOLLOW US ON
					</p>
					<p>Join the conversation and become<br>
						a part of supreme communities.
					</p>
			</div>
		</div>	
			
		
		<div class="col-md-6">
			<div class="col-md-4 footer_contents">
					<p class="footer_title">INFORMATION
					</p>
					<ol>
						<li>Home</li>
						<li>Quality & Tech</li>
						<li>Training</li>
						<li>After Sales Service & Distributors</li>
						<li>News</li>
					</ol>
			</div>
			<div class="col-md-4 footer_contents">
					<p class="footer_title">MY ACCOUNT
					</p>
					<ol>
						<li>About Us</li>
						<li>Customer Care</li>
						<li>Certification</li>
						<li>Contact</li>
						<li>Testimonials</li>
					</ol>
			</div>
			<div class="col-md-4 footer_contents">
					<p class="footer_title">TOOLS
					</p>
					<ol>
						<li>Video</li>
						<li>Media Centre</li>
						<li>Unit Converter</li>
					</ol>
			</div>
		</div>
	</div>
</div>
	</div>
</footer>
<div class="clearfix"></div>
<div id="last_footer" class="container-fluid padding-zero">
	<div class="container foot_wrap">
		<div class="col-md-6">
			<p class="upppercase privacy">PRIVACY POLICY | DISCLAIMER</p>
		</div>
		<div class="col-md-6">
			<p class="copyright uppercase">Copy &copy2016  BALU CRANKSHAFTS . All rights reserved</p>
		</div>
	</div>
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyA3SFi06bFqf80cUhAnQMAmflSpWdjGjVc"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.js"></script>
    <script type="text/javascript" src="js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="js/script.js"></script>

    <script type="text/javascript">
    	$(document).ready(function() {
		  $("#homeBtmSlider").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			loop:true,
			navigation:true,
			items : 3,
			itemsDesktop : [1199,3],
	        itemsDesktopSmall : [980,3],
	        itemsTablet: [768,2],
	        itemsTabletSmall: false,
	        itemsMobile : [479,1]
		  });

		  $("#manuSlider").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			loop:true,
			navigation:true,
			items : 4,
			itemsDesktop : [1199,4],
	        itemsDesktopSmall : [980,4],
	        itemsTablet: [768,3],
	        itemsTabletSmall: false,
	        itemsMobile : [479,2]
		  });
		  
		 
		});
    </script>

		<!-- 				Services
		
		 <div class="container-fluid padding-zero service_bg">
		        <div class="container full_service_wrap">
		          <div class="col-md-12">
		            <div class="col-md-4 ">
		              <div class="col-md-12 service_wrap">
		                <p class="title_1">QUALITY</p>
		                <p class="desc_1">We believe in the spirit of American manufacturing and work with our colleagues, partners, and competitors – along with local educational institutions – to keep good jobs here at home.</p>
		              </div>
		            </div>
		            <div class="col-md-4 ">
		              <div class="col-md-12 service_wrap">
		                <p class="title_1">TECHNOLOGY</p>
		                <p class="desc_1">If you have specific processes, machines, or other requirements, our machining team can help you get the job done. We have expertise in pneumatics, hydraulics, servo systems, and PLCs.</p>
		              </div>
		            </div>
		            <div class="col-md-4 ">
		              <div class="col-md-12 service_wrap">
		                <p class="title_1">INNOVATION</p>
		                <p class="desc_1">It’s not a box to check or form to fill out. It’s how we work. It’s how we live. We continuously push to improve the effectiveness of our ISO 9001:2008 and
		AS9100:2009 Rev C systems.</p>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-12 second_row_service">
		            <div class="col-md-4 ">
		              <div class="col-md-12 service_wrap">
		                <p class="title_1">ENGINEERING</p>
		                <p class="desc_1">For nearly 25 years years, we have led the metal stamping industry designing and building tools, from small, simple forming or stamping to large dies up to 42×72” and 165-ton. Regardless of the size, we are prepared to handle the next job.</p>
		              </div>
		            </div>
		            <div class="col-md-4 ">
		              <div class="col-md-12 service_wrap">
		                <p class="title_1">PRECISION MATCHING</p>
		                <p class="desc_1">Consistency and quality are the hallmarks of our
		precision machining team, delivering the industry’s
		highest tolerances time and time again. Nothing leaves
		our shop before quality and precision have been
		checked and rechecked.</p>
		              </div>
		            </div>
		            <div class="col-md-4 ">
		              <div class="col-md-12 service_wrap">
		                <p class="title_1">ENGINEERING</p>
		                <p class="desc_1">From initial designs and development to final tooling
		packages, our engineering team will take care of every
		detail. And we’ll keep in touch with you at every step
		ensuring your projects are on track.</p>
		              </div>
		            </div>
		          </div>

		        </div>
		      </div> -->


