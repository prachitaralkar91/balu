<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">about us</p>
    		<p class="breadcrumbs">
    			HOME / ABOUT US / <span class="bread_active">about Balu industries</span>
    		</p>
    	</div>
    </div>
    <div class="clearfix"></div>

    <div class="whoweAreSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="title_1 with_line">WHO WE ARE</p>
                        <div class="_line_"><hr></div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="abTopPic">
                                <img src="img/abtPic1.png" width="100%" height="100%">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="abTopPicDesc">
                                Balu’ has become a name symbolising quality & excellence in the field of crankshaft manufacturing since its 
                                inception in 1990.Our in-house capability & state of the art automotive engineering enables us to 
                                manufacture any type of crankshaft in a large range of applications namely Automotive, Agricultural, 
                                Marine & Industrial. We have developed a very extensive range of forged crankshafts for leading Original 
                                Equipment Manufacturers within India and the rest of the world & a strong aftermarket presence in over 80 
                                countries.The ISO/TS16949:2009 accreditation of our units in 2012 by TUV Nord Cert Gmbh added to our 
                                competitive edge making ‘Balu’ one of the very few companies to have this accreditation in the field of 
                                manufacturing crankshafts.<br><br>

                                Balu’ is now an avant-garde manufacturer of fully finished and semi-finished forged crankshafts and 
                                guarantee to manufacture as per customer requirements & market demands. Our incremental innovation & 
                                continuous strive to improve has awarded us by making us the only company to have the capability to 
                                manufacture crankshafts conforming to EURO III / Bharat IV norms.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="homeSect3">
      <div class="container">
        <div class="homeSect3Inner">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12 skil1">
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <img id="awardimg" src="img/abtLogo1.png" style="text-align:center;">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 homeSect3TxtBox">
                  <p class="welcome forfont1">Vision & mission</p>
                  <p>
                    Balu India will strive to be a preferred supplier of crankshafts 
                    to OEM's in India and around the world. We will also target to be 
                    a leading brand of crankshafts in the aftermarket segment.
                  </p>
                  <button class="btn btn-danger" id="rm12" onclick="openRMmodal(this.id);">
                    Read More
                  </button>
                </div>

                <div class="readMoreInfo" id="rm121" style="display:none;">
                  <h3>Vision & mission</h3>
                  <div class="clearfix"></div>
                  <div class="sub_txt">
                    ‘Balu’ will continuously strive to be a preferred supplier of crankshafts 
                    to OEM's in India and around the world. ‘Balu’ aims to enhance and grow 
                    our reputation as one of the world's most respected manufacturing 
                    companies by exceeding customer expectations, providing an engaging 
                    and supportive work environment, and delivering financial success 
                    and opportunities for our stakeholders. We aim to always UNDERPROMISE 
                    & OVERDELIVER in all our ventures. <br><br>
                    While pursuing the above, we will ensure that we establish a robust 
                    management system so as to enhance customer's experience in dealing 
                    with us, satisfaction of all stakeholders and due consideration to the environment.

                  </div>
                </div>
              </div>

              <div class="col-md-4 col-sm-4 col-xs-12 skil1">
                <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:center">
                  <img id="crsimg" src="img/abtLogo2.png">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 homeSect3TxtBox">
                  <p class="welcome forfont1">CULTURE</p>
                  <p>
                    We value each other, our customers, our business partners, and our environment.
                  </p>
                  <button class="btn btn-danger" id="rm13" onclick="openRMmodal(this.id);">
                    Read More
                  </button>
                </div>

                <div class="readMoreInfo" id="rm131" style="display:none;">
                  <h3>CULTURE</h3>
                  <div class="clearfix"></div>
                  <div class="sub_txt">
                    <b>Respect:</b> We value each other, our customers, our business partners, 
                    and our environment. <br><br> 
                    <b>Honesty:</b> We are genuine and open in our communication and business 
                    practices.   <br>  <br>
                    <b>Commitment to Quality:</b> We deliver products that exceed our customer's 
                    expectations.   <br><br>
                    <b>Creativity:</b> We listen, encourage and support different approaches 
                    as we continually strive to improve <br><br>
                    <b>Growth:</b> We invest in personal and professional development <br><br>
                    <b>Teamwork:</b> We work together towards a shared goal 

                  </div>
                </div>


              </div>

              <div class="col-md-4 col-sm-4 col-xs-12 skil1">
                <div class="col-md-4 col-sm-4 col-xs-12" style="text-align:center">
                  <img id="newsimg" src="img/abtLogo3.png">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 homeSect3TxtBox">
                  <p class="welcome forfont1">CORE COMPETENCY</p>
                  <p>
                    The Core Competencies which distinguish 'Balu' from its competitors 
                    & pillars which have ensured the building of a strong brand worldwide 
                    are 
                  </p>
                  <button class="btn btn-danger" id="rm14" onclick="openRMmodal(this.id);">
                    Read More
                  </button>
                </div>

                <div class="readMoreInfo" id="rm141" style="display:none;">
                  <h3>CORE COMPETENCY</h3>
                  <div class="clearfix"></div>
                  <div class="sub_txt">
                    The Core Competencies which distinguish ‘Balu’ from its competitors 
                    & the pillars which have ensured the building of a strong brand 
                    worldwide are as below:<br><br>
                    <ul style="list-style-type:disc">
                      <li style="list-style-type:disc">Complete CNC manufacturing capability</li>
                      <li style="list-style-type:disc">1000 Crankshaft manufacturing capacity per day</li>
                      <li style="list-style-type:disc">Crankshaft applications upto 2.5 meter in length</li>
                      <li style="list-style-type:disc">Over 25 years of experience in crankshaft manufacturing</li>
                      <li style="list-style-type:disc">Customer base in over 80 countries</li>
                      <li style="list-style-type:disc">Company of choice for OEMs</li>
                      <li style="list-style-type:disc">State of the Art Manufacturing set up</li>
                      <li style="list-style-type:disc">Unconditional guarantee of 1 year for all crankshafts manufactured</li>
                      <li style="list-style-type:disc">Largest Crankshaft Range</li>
                      <li style="list-style-type:disc">Unmatched range of crankshafts manufactured from forgings</li>
                      <li style="list-style-type:disc">In-house tool room</li>
                      <li style="list-style-type:disc">In-house Metallurgical lab</li>
                      <li style="list-style-type:disc">All machining operations in-house</li>
                    </ul>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>


    <div class="companyHisSect">
      <div class="container">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="title_1 with_line">COMPANY HISTORY</p>
                <div class="_line_"><hr></div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="abTopPicDesc text-center" style="margin-bottom:15px;">
                ‘Balu’ had a humble beginning in the year 1990, in Belgaum, India. The initial set up was for manufacturing of crankshafts 
                for single cylinder & two cylinder with a  limited workforce. Within the last decade the company is supplying to OEM 
                companies and the aftermarket presence had since expanded to over 80 countries. In 1990, Balu India was the first 
                company in India to mass-produce Crankshafts suitable for Tractor, Trucks and Passenger car applications. Two decades 
                later ‘Balu’ has gained excellent reputation in the world market. The modern era of ‘Balu’ upholds the reputation 
                of durability by manufacturing Crankshafts only out of Forgings.
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="wpb_wrapper">
                <div class="dt-timeline">
                  <div class="timeline-left-group">
                    <div class="time-item left">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_single_image wpb_content_element vc_align_left default">
                            <div class="wpb_wrapper"> 
                              <img width="800"  src="img/abtPic2.png" class=" vc_box_border_grey attachment-full" alt="history1"> </div>
                          </div>
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">1990</span>
                              
                              <p>The foundation of 'Balu' was laid by Mr.Prahlad Singh Chandock</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-item left">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2006</span>
                              <p>Purchase & Installation of the Ursus Manufacturing Plant from Ursus, Poland</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-item left">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2010</span>
                              <p>Purchase & Installation of the Thyssenkrupp Plant from L'horme, France</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-item left">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_single_image wpb_content_element vc_align_left default">
                            <div class="wpb_wrapper"> <img class=" vc_box_border_grey " src="img/abtPic2.png" width="500"  alt="history4" title="history4"> </div>
                          </div>
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2012</span>
                              
                              <p>Got accredited with ISO/TS16949:2009 accreditation from world renowned company TUV NORD</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="timeline-right-group">
                    <div class="time-item right">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">1991</span>
                              
                              <p>First crankshaft manufactured</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-item right">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">1995</span>
                              
                              <p>First Crankshaft exported to an overseas market</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-item right">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_single_image wpb_content_element vc_align_left default">
                            <div class="wpb_wrapper"> <img class=" vc_box_border_grey " src="img/abtPic2.png" width="500" alt="history3" title="history3"> </div>
                          </div>
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2000</span>
                              
                              <p>2004 ACMA export award for excellence in exports </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="time-item right">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2013</span>
                              
                              <p>Coined our very own ‘Concurrent Engineering’ to achieve better flexibility & speed in Development projects
</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="time-item right">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2014</span>
                              
                              <p>Achieved manufacturing of 1000 crankshafts in a single day</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="time-item right">
                      <div class="center-line circle"><i></i></div>
                      <div class="content-line">
                        <div class="content-text">
                          <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                              <span class="primary_color_text">2015</span>
                              
                              <p>Manufacture & Supply of crankshaft of 2.5 metres for Railways & Military application</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
            </div>



          </div>
        </div>
      </div>
    </div>


    <div class="clearfix"></div>


    <div class="teamSect">
      <div class="container">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <p class="title_1 with_line">MEET OUR TEAM</p>
                <div class="_line_"><hr></div>
            </div>
            <div class="col-md-12">
              <div class="abTopPicDesc text-center">
                Balu’s board of directors plays a critical role in the governing of the business. Its diversity lends important 
                perspective and depth to the group’s direction. We are therefore committed to building a board that is diverse in 
                terms of role and experience.
              </div>
            </div>
          </div>
          <div class="row">
            <div class="teamOuter">
              <div class="col-md-4">
                <div class="teamGridBox">
                  <div class="teamPic text-center">
                    <img src="img/teamPic1.png">
                  </div>
                  <div class="teamName">
                    Mr. Jaspal Singh Chandock
                  </div>
                  <div class="teamPost">
                    Managing Director
                  </div>
                  <div class="abTopPicDesc">
                    A Mumbai based & 2nd generation entrepreneur with decades of experience & investments in a vast sphere of industries. 
                    The Foundation for ‘Balu’ was laid by Mr. Jaspal Singh Chandock under the guidance of late Mr. Prehlad Singh Chandock. 
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="teamGridBox">
                  <div class="teamPic text-center">
                    <img src="img/teamPic2.png">
                  </div>
                  <div class="teamName">
                    Mr. Trimaan Singh Chandock
                  </div>
                  <div class="teamPost">
                    Executive Director
                  </div>
                  <div class="abTopPicDesc">
                    A young & dynamic leader with a MSc & a BSc in Management studied from H.R College, Mumbai. A 3rd generation 
                    entrepreneur who joined the company in 2009.  A visionary with a keen interest in innovation in the field of manufacturing.
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="teamGridBox">
                  <div class="teamPic text-center">
                    <img src="img/teamPic3.png">
                  </div>
                  <div class="teamName">
                    Mr. Jaikaran Singh Chandock
                  </div>
                  <div class="teamPost">
                    Executive Director
                  </div>
                  <div class="abTopPicDesc">
                    The youngest & newest recruit who joined the business in 2014 after completion of BSc in Business Management from Cass 
                    Business School, London & MSc in Strategic Marketing from Imperial College, London. 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <?php include "footer.php"; ?>
  </body>
</html>   