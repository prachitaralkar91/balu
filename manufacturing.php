<!DOCTYPE html>
<html lang="en">
    <?php include 'head.php'; ?>
  <body>
    <?php include "header.php"; ?>
    <div class="clearfix"></div>
    <div class="header_promo">
    	<div class="heading_of_promo">
    		<p class="heading_of_promo_p">Manufacturing</p>
    		<p class="breadcrumbs">
    			HOME / <span class="bread_active">MANUFACTURING</span>
    		</p>
    	</div>
    </div>
    <div class="clearfix"></div>

    <div class="appliSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title_1 with_line">OUR APPLICATIONS </p>
                        <div class="_line_"><hr></div>
                    </div>
                    <div class="col-md-12">
                        <div class="abTopPicDesc text-center">
                            ‘BALU’ has supplied high-quality crankshafts for over 25 years. The continuously increasing demand for 
                            our products and developments ensures that we constantly create future-oriented solutions. Our customers 
                            include some of the most renowned suppliers and manufacturers of prototypes, high performance vehicles,  
                            commercial vehicles, off-highway vehicles, ships and locomotives.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="appliSliderBox" id="manuSlider">
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic1.png" width="40" height="49">
                                    <p>AGRICULTURE</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic2.png" width="58" height="50">
                                    <p> CAR MANUFACTURERS</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic3.png" width="123" height="49">
                                    <p>EARTHMOVING EQUIPMENT</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic4.png" width="125" height="49">
                                    <p>INDUSTRIAL VEHICLES</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic1.png" width="40" height="49">
                                    <p>AGRICULTURE</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic2.png" width="58" height="50">
                                    <p> CAR MANUFACTURERS</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic3.png" width="123" height="49">
                                    <p>EARTHMOVING EQUIPMENT</p>
                                </div>
                            </div>
                            <div class="item col-md-12">
                                <div class="appligridbox text-center">
                                    <img src="img/manuSliderPic4.png" width="125" height="49">
                                    <p>INDUSTRIAL VEHICLES</p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="processSect">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <p class="title_1 with_line">OUR PROCESSES</p>
                        <div class="_line_"><hr></div>
                    </div>
                    <div class="col-md-12">
                        <div class="abTopPicDesc text-center">
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                            a galley of type and <br>scrambled it to make a type specimen book.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="processSectOuter">
                        <div class="col-md-3">
                            <img src="img/manuPic1.png" width="100%" height="150">
                        </div>
                        <div class="col-md-9">
                            <div class="processTitle">
                                FORGING
                            </div>
                            <div class="abTopPicDesc">
                                The process of forging is carried out on forging hammers with a capacity of upto 16000 T & presses 
                                with a capacity of 4000 T. The metal is shaped in the form of a crankshaft by hammering the heated 
                                cut piece between the top and the bottom dies fixed on the hammer. The finished product through this 
                                process is stronger and superior in quality than a similar product produced by process of casting. 
                                The forgings then undergo further processes such as heat treatment, shot blasting and magnaflux crack 
                                detection as per customer requirement. 
                            </div>
                            <div class="readBtn" id="rm15" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>

                        <div class="readMoreInfo" id="rm151" style="display:none;">
                          <h3>FORGING</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                            <img src="img/manuPic1.png" width="255" height="150">
                        </div>
                          <div class="sub_txt">
                            The Forging Process<br><br>
                            The process of forging is carried out on forging hammers with 
                            a capacity of upto 16000 T & presses with a capacity of 4000 T. 
                            The metal is shaped in the form of a crankshaft by hammering 
                            the heated cut piece between the top and the bottom dies fixed 
                            on the hammer. The finished product through this process is 
                            stronger and superior in quality than a similar product produced 
                            by process of casting. The forgings then undergo further 
                            processes such as heat treatment, shot blasting and magnaflux 
                            crack detection as per customer requirement. <br><br>
                            A Forged crankshaft is recommended for high power transmission 
                            and high RPM applications whereas a cast crankshaft can tolerate 
                            less power. <br><br>
                            The company is equipped with an in-house die making capability, 
                            trimming press & heat treatment to eliminate the need for 
                            outsourcing. There is an annual capacity of 30000 tons per year 
                            for crankshaft forgings <br><br>
                            <b>Advantages </b><br><br>
                            The Forged crankshafts provide strength and reliability that 
                            far surpass any cast or turned bar stock, making forged 
                            crankshafts the standard for any use that demands strength, 
                            consistency or quality. The general advantages of any forged 
                            product over a casting one are as follow:<br>
                            <ul>
                                <li>Greater strength</li>
                                <li>Finer grain size</li>
                                <li>Improved microstructure</li>
                                <li>Better fatigue resistance</li>
                                <li>Reduced chance of voids</li>
                                <li>Continuous grain flow</li>
                            </ul>
 
                            </div>
                        </div>
                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-9">
                            <div class="processTitle">
                                STRESS RELIEVING
                            </div>
                            <div class="abTopPicDesc">
                                Stress relieving is done by subjecting the crankshaft to a temperature of about 650 ºC (1202 ºF) for about one hour or till the whole 
                                part reaches the temperature. This temperature can be adjusted as per customer requirement  This removes more than 90% of the internal 
                                stresses.
                            </div>
                            <div class="readBtn" id="rm16" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>
                        <div class="col-md-3">
                            <img src="img/manuPic2.png" width="100%" height="150">
                        </div>

                        <div class="readMoreInfo" id="rm161" style="display:none;">
                          <h3>STRESS RELIEVING</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                            <img src="img/manuPic2.png" width="100%" height="150">
                        </div>
                          <div class="sub_txt">
                            Stress relieving is done by subjecting the crankshaft to a 
                            temperature of about 650 ºC (1202 ºF) for about one hour or 
                            till the whole part reaches the temperature. This temperature 
                            can be adjusted as per customer requirement. This removes more 
                            than 90% of the internal stresses.<br><br>

                            <b>SERRATION </b><br>
                            A very special CNC special purpose machine is utilized for 
                            the purpose of producing the Serration on the gear end of 
                            specific crankshafts which may have a requirement for the 
                            same The process is called "deep rolling on serration".<br><br>

                            <b>POST GRINDING</b><br>
                            All post grinding operations are carried out on our 4-Axis HMC 
                            (Horizontal Machining Centre) & VMC (Vertical Machining Centre). 
                            The presence of Makino offers the competitive edge & shorter 
                            lead times with greater accuracy in crankshaft post grinding 
                            operations. <br><br>

                            <b>DYNAMIC BALANCING</b><br>
                            The sum of these processes optimises the running behaviour of 
                            the crankshaft in the engine and ensures that natural 
                            vibrations are as low as possible. Our computer-controlled
                            balancing machines can dynamically balance crankshafts with a 
                            length of up to 2.5 m.<br><br>

                            <b>CNC GRINDERS</b><br>
                            The presence of the state of the Art Newall grinders on our 
                            machining line offers an advantage in terms of speed & 
                            accuracy. The infrastructure consists of 20 Newall grinders 
                            which were purchased from the Ursus factory in Poland.
                            These grinders with a large capacity enable the company to 
                            produce 1000 fully finished crankshafts on each working day.<br><br>


                            <b>SUPER FINISHING</b><br>
                            The surface finish is a key process to ensure the smooth 
                            running of the engine. All crankshafts undergo the process 
                            of auto lapping on the Nagel machinery to achieve a surface 
                            finish of 1.5RA TO 2RA or if there is any specific requirement 
                            of a customer.<br><br>

                            <b>ACCESSORY PRODUCTION AND ASSEMBLY</b><br>
                            We do not only manufacture & supply crankshafts, we supply 
                            ready-to-install crankshaft for manufacturers. This also 
                            includes the production and assembly of counterweights and 
                            crankshaft gears. The same is done very carefully & checked<br><br>


                            <b>QUALITY CONTROL</b><br>
                            Each crankshaft is inspected for quality throughout the 
                            manufacturing process. Once the final process is complete, 
                            our crankshafts undergo a thorough final inspection. The 
                            specialised & trained technicians perform an exhaustive 
                            physical inspection of each part, ensuring the high standard 
                            of quality that our customers demand. As per customer demand 
                            all reports namely metallurgical, dimensional,etc are provided 
                            with every batch or lot supplied.<br><br>

                            <b>R&D Laboratory & Testing Centre</b><br>
                            ‘Balu’ is equipped with an R&D Laboratory & testing centre. 
                            It helps in verifying materials, trouble-shooting production 
                            problems, investigating product failures and meeting customer 
                            requirements.. We offer material testing services to give you 
                            all the information you need about your materials and products:
                            <br> 
                            <ul>
                                <li>Material composition</li>
                                <li>Verification of mechanical properties</li>
                                <li>Microscopic product feature evaluation</li>
                                <li>Flaw Detection</li>
                                <li>Corrosion susceptibility</li>
                            </ul> 


                          </div>
                        </div>


                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-3">
                            <img src="img/manuPic3.png" width="100%" height="150">
                        </div>
                        <div class="col-md-9">
                            <div class="processTitle">
                                METALLURGICAL TESTING
                            </div>
                            <div class="abTopPicDesc">
                                Metallurgical testing includes microhardness testing and microscopic and macroscopic examinations to 
                                evaluate surface and internal features, defects and material characteristics. Samples for all metallurgical 
                                testing are prepared at our laboratory
                                There is an in-house metallur gical lab where inward material is tested & also per lot one unit is cut 
                                to check the pattern of the hardness & material flow. The infrastructure enables conducting metallurgical 
                                tests in terms of case depth, surface hardness, etc.
                            </div>
                            <div class="readBtn" id="rm17" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>

                        <div class="readMoreInfo" id="rm171" style="display:none;">
                          <h3>METALLURGICAL TESTING</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                            <img src="img/manuPic3.png" width="100%" height="150">
                        </div>
                          <p class="sub_txt">
                            Metallurgical testing includes microhardness testing and 
                            microscopic and macroscopic examinations to evaluate surface 
                            and internal features, defects and material characteristics. 
                            Samples for all metallurgical testing are prepared at our 
                            laboratory.<br><br>

                            There is an in-house metallurgical lab where inward material 
                            is tested & also per lot one unit is cut to check the pattern 
                            of the hardness & material flow. The infrastructure enables 
                            conducting metallurgical tests in terms of case depth, surface 
                            hardness, etc.<br><br>
   
                            <b>MAGNAFLUX CRACK TESTING</b><br>
                            The Magnaflux method is utilized to identify cracks in 
                            crankshafts. At ‘Balu’ a 100% Crack testing is carried out on 
                            each & every product. The Magnaflux process utilizes an 
                            electrical current to produce magnetism in the part being 
                            checked. The crankshaft having been magnetized will expose 
                            any hidden cracks on the crankshaft & utilizing the ultraviolet 
                            light clearly shows the flow crack. The Magnaflux process is a 
                            critical step many cracks would otherwise go unnoticed. 
                            The cracks present in crankshafts are often not visible to 
                            the naked eye.

                          </p>
                        </div>


                    </div>

                    <div class="processSectOuter">
                        <div class="col-md-9">
                            <div class="processTitle">
                                DURABILITY TESTing
                            </div>
                            <div class="abTopPicDesc">
                                It determines convenience of metal material’s mechanical feature. We use both In-house & outsourced 
                                equipment to conduct fatigue tests as per customer requirement. The equipment used to carry out the 
                                fatigue test is pulsator & an electrodynamic shaker.
                            </div>
                            <div class="readBtn" id="rm18" onclick="openRMmodal(this.id);">READ MORE</div>
                        </div>
                        <div class="col-md-3">
                            <img src="img/manuPic4.png" width="100%" height="150">
                        </div>

                        <div class="readMoreInfo" id="rm181" style="display:none;">
                          <h3>DURABILITY TESTing</h3>
                          <div class="clearfix"></div>
                          <div class="rdImg">
                            <img src="img/manuPic4.png" width="100%" height="150">
                        </div>
                          <div class="sub_txt">
                            It determines convenience of metal material’s mechanical 
                            feature. We use both In-house & outsourced equipment to 
                            conduct fatigue tests as per customer requirement. 
                            The equipment used to carry out the fatigue test is pulsator 
                            & an electrodynamic shaker. <br><br>
                
                            <b>PACKAGING & STORAGE</b><br>
                            We offer two solutions namely to our customer namely 
                            Individual & Bulk packaging solutions. An In-house packaging 
                            unit provides flexibility to customise packaging as per 
                            customer requirement. We provide Anti-rust solutions on the 
                            finished crankshafts with choices of Wax or Oil as per customer 
                            demand & feedback. <br><br>

                            In addition to the above, our In-house setup serves us with 
                            the opportunity to serve our customers with any wooden 
                            packaging requirement they may have. This can be manufactured 
                            as per customer size & requirement.

                          </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
  </body>
</html>  